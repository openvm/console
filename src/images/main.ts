import { autoinject } from 'aurelia-framework'
import { Images } from './../api/images'

@autoinject
export class List {
  constructor(private images: Images) {

  }

  public async activate(): Promise<void> {
    return this.images.update()
  }
}
