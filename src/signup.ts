import {autoinject} from 'aurelia-framework'
import {Router} from 'aurelia-router'
import { User } from './api/user'

@autoinject
export class Signup {
  public username: string
  public password: string
  public email: string

  constructor(private router: Router, private user: User) {

  }

  public signup(): void {
    this.user.signup(this.username, this.password, this.email).then((res) => {
      this.router.navigate('machines')
    })
  }
}
