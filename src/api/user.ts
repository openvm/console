import { AureliaConfiguration } from 'aurelia-configuration'
import { EventAggregator } from 'aurelia-event-aggregator'
import { json } from 'aurelia-fetch-client'
import { autoinject } from 'aurelia-framework'
import { url } from 'gravatar'
import jwtDecode from 'jwt-decode'
import { Base } from './base'

@autoinject
export class User extends Base {
  public token: string
  public name: string
  public email: string
  public gravatar: string
  public loggedIn: boolean

  constructor(private event: EventAggregator, config: AureliaConfiguration) {
    super(config)
    this.token = sessionStorage.getItem('token')
    this.updateLoginStatus()
    if (this.loggedIn) {
      this.updateUserInfo()
      this.event.publish('api:user:login')
    } else {
      // remove expired/invalid token
      sessionStorage.removeItem('token')
    }
  }

  public update(): Promise<void> {
    return this.fetch('GET', '/user')
      .then(({ data }) => {
        this.name = data.attributes.name
        this.email = data.attributes.email
      })
  }

  public login(name: string, password: string): Promise<void> {
    return this.fetch('POST', '/user/login', {
      body: json({
        data: {
          attributes: {
            claims: ['email'],
            name,
            password,
          },
        },
      }),
    }).then(({data}) => {
      this.token = data.attributes.token
      sessionStorage.setItem('token', this.token)
      this.updateLoginStatus()
      this.updateUserInfo()
      this.event.publish('api:user:login')
    })
  }

  public signup(name: string, password: string, email: string): Promise<void> {
    return this.fetch('POST', '/user', {
      body: json({
        data: {
          attributes: {
            email,
            name,
            password,
          },
          type: 'users',

        },
      }),
    }).then((res) => {
      return this.login(name, password)
    })
  }

  public logout(): void {
    sessionStorage.removeItem('token')
    this.token = undefined
    this.updateLoginStatus()
    this.event.publish('api:user:logout')
  }

  private updateLoginStatus(): void {
    if (!this.token) {
      this.loggedIn = false
      return
    }

    let claims

    try {
      claims = jwtDecode(this.token)
    } catch {
      this.loggedIn = false
      return
    }

    if (claims.exp < (Date.now() / 1000)) {
      this.loggedIn = false
      return
    }

    this.loggedIn = true
  }

  private updateUserInfo(): void {
    const claims = jwtDecode(this.token)
    this.name = claims.name
    this.email = claims.email
    this.gravatar = url(claims.email, {s: '150', d: 'retro'})
  }

}
