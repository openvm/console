import { AureliaConfiguration } from 'aurelia-configuration'
import { EventAggregator } from 'aurelia-event-aggregator'
import { json } from 'aurelia-fetch-client'
import { autoinject } from 'aurelia-framework'
import { url } from 'gravatar'
import jwtDecode from 'jwt-decode'
import { Base } from './base'

export interface IImage {
  id: string
  aliases: IImageAliases[]
  properties: IImageProperties
}

export interface IImageAliases {
  name: string
  description: string
}

export interface IImageProperties {
  architecture: string
  description: string
  os: string
  release: string
  serial: string
}


@autoinject
export class Images extends Base {
  public all: IImage[]

  constructor(config: AureliaConfiguration) {
    super(config)
  }

  public update(): Promise<void> {
    return this.fetch('GET', '/images')
      .then(({ data }) => {
        this.all = data.map((i) => ({ id: i.id, ...i.attributes }))
      })
  }
}
