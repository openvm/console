import { AureliaConfiguration } from 'aurelia-configuration'
import { json } from 'aurelia-fetch-client'
import { autoinject } from 'aurelia-framework'
import { Base } from './base'

export interface IMachine {
  id: number
  name: string
  image: string
  driver: string
  status: string
}

@autoinject
export class Machines extends Base {
  public all: IMachine[]

  constructor(config: AureliaConfiguration) {
    super(config)
  }

  public get(id: number): IMachine {
    return this.all.find((m) => m.id === id)
  }

  public find(predicate: (value: IMachine) => boolean): IMachine {
    return this.all.find(predicate)
  }

  public update(): Promise<void> {
    return this.fetch('GET', '/machines')
      .then(({ data }) => {
        this.all = data.map((i) => ({ id: i.id, ...i.attributes }))
      })
  }

  public create(name: string, image: string): Promise<IMachine> {
    return this.fetch('POST', '/machines', {
      body: json({
        data:
        {
          attributes: {
            image,
            name,
          },
          type: 'machines',
        },
      }),
    }).then(({ data }) => {
      const machine = { id: data.id, ...data.attributes }
      this.all.push(machine)
      return machine
    })
  }

  public delete(id: number): Promise<void> {
    return this.fetch('DELETE', `/machines/${id}`)
      .then((_) => {
        this.all = this.all.filter((m) => m.id !== id)
      })
  }

  public status(id: number, status: string): Promise<void> {
    return this.fetch('POST', `/machines/${id}`, {
      body: json({
        data:
        {
          attributes: {
            status,
          },
          type: 'machines',
        },
      }),
    }).then(() => {
      this.get(id).status = status
    })
  }

}
