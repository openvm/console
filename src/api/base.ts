import { AureliaConfiguration } from 'aurelia-configuration'
import { HttpClient, RequestInit } from 'aurelia-fetch-client'

const networkError = {
  errors: [
    {
      detail: 'Please check your internet connection.',
      title: 'Network Error',
    },
  ],
}

export class Base {
  public http: HttpClient = new HttpClient()

  constructor(config: AureliaConfiguration) {
    this.http.configure((httpConfig) => {
      httpConfig
        .useStandardConfiguration()
        .withBaseUrl(config.get('api.endpoint'))
    })
  }

  public fetch<T>(method: string, url: string, opts: RequestInit = {}): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      const headers = {
        Accept: 'application/vnd.api+json',
        Authorization: '', // TODO needed to fix ts error
        'Content-Type': 'application/vnd.api+json',
      }

      const token = sessionStorage.getItem('token')
      if (token) {
        headers.Authorization = `Bearer ${token}`
      }

      this.http.fetch(url, { method, headers, ...opts })
        .then((res) => {
          if (!res.ok) {
            return res.json().then(reject)
          }
          if (res.status === 204) {
            return resolve()
          }
          res.json().then(resolve)
        }).catch(() => reject(networkError))
    })
  }
}
