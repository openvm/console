import 'bulma/css/bulma.css'
import 'font-awesome/css/font-awesome.css'
import 'font-mfizz/dist/font-mfizz.css'
import 'xterm/dist/xterm.css'

import { Aurelia } from 'aurelia-framework'
import { PLATFORM } from 'aurelia-pal'
import '../static/styles.css'

export async function configure(aurelia: Aurelia): Promise<void> {
  aurelia.use
    .standardConfiguration()
    .developmentLogging()
    .plugin(PLATFORM.moduleName('aurelia-configuration'), (config) => {
      config.setDirectory('.')
    })

  await aurelia.start()
  await aurelia.setRoot(PLATFORM.moduleName('main'))
}
