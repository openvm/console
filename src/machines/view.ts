import { AureliaConfiguration } from 'aurelia-configuration'
import { EventAggregator } from 'aurelia-event-aggregator'
import { autoinject } from 'aurelia-framework'
import { IMachine, Machines } from '../api/machines'
import { User } from './../api/user'

@autoinject
export class View {
  public machine: IMachine

  private poweroffModal: Element

  private showAdvanced: boolean = false

  constructor(
    private config: AureliaConfiguration,
    private event: EventAggregator,
    private machines: Machines,
    private user: User) {

  }

  public activate({ id }: { id: number }): void {
    this.machine = this.machines.get(id)
  }

  public delete(): void {
    this.machines.delete(this.machine.id)
  }

  public connect(id: number): void {
    this.event.publish('ui:machines:connect', { machine: this.machine })
  }

  public toggleModal(el: Element): void {
    el.classList.toggle('is-active')
  }

  private toggleAdvanced(): void {
    this.showAdvanced = !this.showAdvanced
  }

  private status(status: string): void {
    this.machines.status(this.machine.id, status)
  }

  private poweroff(): void {
    this.status('stopped')
    this.toggleModal(this.poweroffModal)
  }
}
