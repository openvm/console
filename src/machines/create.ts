import { AureliaConfiguration } from 'aurelia-configuration'
import { autoinject } from 'aurelia-framework'
import { Router } from 'aurelia-router'
import dockerNames from 'docker-names'
import { Machines } from '../api/machines'

interface IImage {
  slug: string
  name: string
  versions: IImageVersion[]
}

interface IImageVersion {
  name: string
  lxd: string
}

@autoinject
export class Create {
  private name: string
  private images: IImage[] = this.config.get('images')
  private image: string = this.images[0].slug
  private version: string = this.images[0].versions[0].name

  constructor(private router: Router, private machines: Machines, private config: AureliaConfiguration) {
    this.randomName()
  }

  public randomName(): void {
    this.name = dockerNames.getRandomName().replace('_', '-')
  }

  public create(): void {

    const version = this.images.find((i) => i.slug === this.image).versions.find((v) => v.name === this.version)
    this.machines.create(this.name, version.lxd)
      .then((machine) => this.router.navigateToRoute('view', {id: machine.id}))
  }
}
