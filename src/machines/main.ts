import { autoinject } from 'aurelia-framework'
import { PLATFORM } from 'aurelia-pal'
import { Router, RouterConfiguration } from 'aurelia-router'
import { Machines } from '../api/machines'
import { User } from './../api/user'

@autoinject
export class Main {
  public router: Router

  constructor(private machines: Machines, private user: User) {
  }

  public async activate(): Promise<void> {
    if (!this.user.loggedIn) {
      this.router.navigateToRoute('login')
      return
    }
    return this.machines.update()
  }

  public configureRouter(config: RouterConfiguration, router: Router): void {
    config.map([
      {
        moduleId: PLATFORM.moduleName('./list'),
        name: 'list',
        route: ['', 'list'],
        title: 'All Machines',
      },
      {
        moduleId: PLATFORM.moduleName('./create'),
        name: 'create',
        route: 'create',
        title: 'Create Machine',
      },
      {

        moduleId: PLATFORM.moduleName('./view'),
        name: 'view',
        route: 'view/:id',
        title: 'View Machine',
      },
    ])

    this.router = router
  }
}
