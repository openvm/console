import { AureliaConfiguration } from 'aurelia-configuration'
import { EventAggregator, Subscription } from 'aurelia-event-aggregator'
import { autoinject } from 'aurelia-framework'
import { debounce } from 'lodash'
import RobustWebsocket from 'robust-websocket'
import { IMachine } from './../api/machines'
import { Terminal as XTerm } from 'xterm'
import * as attach from 'xterm/lib/addons/attach/attach'
import * as fit from 'xterm/lib/addons/fit/fit'

XTerm.applyAddon(attach);
XTerm.applyAddon(fit);

@autoinject
export class Terminal {
  public machine: IMachine

  private io: RobustWebsocket
  private control: RobustWebsocket

  private xterm: XTerm
  private el: HTMLElement

  private subscriptions: Subscription[] = []

  private fit: () => void = debounce(() => this._fit(), 100)

  constructor(private event: EventAggregator, private config: AureliaConfiguration) {

  }

  public focus(): void {
    this.xterm.focus()
  }

  private activate(machine: IMachine): void {
    this.machine = machine
  }

  private _fit(): void {
    const parent = document.querySelector('#bottom-pane')
    const parentStyle = window.getComputedStyle(parent)
    const parentHeight = parseInt(parentStyle.getPropertyValue('height'), 10)

    const tabs = document.querySelector('#bottom-pane .tabs')
    const tabsStyle = window.getComputedStyle(tabs)
    const tabsHeight = parseInt(tabsStyle.getPropertyValue('height'), 10)

    const height = `${parentHeight - tabsHeight}px`

    if (this.el.style.height === height) {
      return
    }

    this.el.style.height = height;
    (this.xterm as any).fit()
  }

  private attached(): void {
    const baseUrl = this.config.get('api.endpoint.').replace('http', 'ws')
    const query = `?access_token=${sessionStorage.getItem('token')}`
    this.io = new RobustWebsocket(`${baseUrl}/machines/${this.machine.id}/io${query}`)
    this.control = new RobustWebsocket(`${baseUrl}/machines/${this.machine.id}/control${query}`)

    this.xterm = new XTerm();
    (this.xterm as any).attach(this.io)
    this.xterm.open(this.el)

    this.control.addEventListener('open', () => {
      this.subscriptions.push(
        this.event.subscribe('ui:pane:drag:end', this.fit),
      )
      window.addEventListener('resize', this.fit)

      this.xterm.on('resize', (size) => {
        this.control.send(JSON.stringify({
          args: {width: size.cols.toString(), height: size.rows.toString()},
          command: 'window-resize',
        }))
      })

      this._fit()
    })
  }

  private detached(): void {
    this.subscriptions.forEach((s) => s.dispose())
    window.removeEventListener('resize', this.fit)
    this.xterm.destroy()
    this.io.close()
    this.control.close()
  }

}
