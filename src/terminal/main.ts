import { EventAggregator, Subscription } from 'aurelia-event-aggregator'
import { autoinject } from 'aurelia-framework'
import { IMachine, Machines } from './../api/machines'
import { User } from './../api/user'
import { Terminal } from './terminal'

interface ISession extends IMachine {
  viewModel: IComposeViewModel
}

interface IComposeViewModel {
  currentViewModel: Terminal
}

@autoinject
export class Main {
  private sessions: ISession[] = []
  private active: ISession
  private subscriptions: Subscription[] = []

  constructor(private event: EventAggregator, private user: User, private machines: Machines) {
  }

  private attached(): void {
    this.subscriptions.push(
      this.event.subscribe('ui:machines:connect', ({ machine }) => {
        const existing = this.sessions.find((m) => m.id === machine.id)
        if (existing) {
          this.active = existing
          setTimeout(() => this.active.viewModel.currentViewModel.focus(), 0)
        } else {
          this.sessions.push(machine)
          this.active = machine
        }
      }),
      this.event.subscribe('api:user:logout', () => {
        this.active = undefined
        this.sessions = []
      }),
    )
  }

  private detached(): void {
    this.subscriptions.forEach((s) => s.dispose())
  }

  private setActive(index: number): void {
    this.active = this.sessions[index]
    setTimeout(() => this.active.viewModel.currentViewModel.focus(), 0)
  }
}
