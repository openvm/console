import { AureliaConfiguration } from 'aurelia-configuration'
import { EventAggregator, Subscription } from 'aurelia-event-aggregator'
import { autoinject } from 'aurelia-framework'
import { PLATFORM } from 'aurelia-pal'
import { Router, RouterConfiguration } from 'aurelia-router'
import Split from 'split.js'
import { User } from './api/user'

@autoinject
export class Main {
  private router: Router
  private split: Split
  private showBottomPane: boolean = false
  private subscriptions: Subscription[] = []

  constructor(private event: EventAggregator,
              private config: AureliaConfiguration,
              private user: User) {

  }

  private configureRouter(config: RouterConfiguration, router: Router): void {
    config.title = this.config.get('application.name')
    config.map([
      {
        moduleId: PLATFORM.moduleName('./home'),
        name: 'home',
        route: '',
        title: 'Home',
      },
      {
        moduleId: PLATFORM.moduleName('./login'),
        name: 'login',
        route: 'login',
        title: 'Login',
      },
      {
        moduleId: PLATFORM.moduleName('./signup'),
        name: 'signup',
        route: 'signup',
        title: 'Signup',
      },
      {
        moduleId: PLATFORM.moduleName('./machines/main'),
        name: 'machines',
        route: 'machines',
        title: 'Machines',
      },
      {
        moduleId: PLATFORM.moduleName('./user/main'),
        name: 'user',
        route: 'user',
        title: 'User Settings',
      },
      {
        moduleId: PLATFORM.moduleName('./images/main'),
        name: 'images',
        route: 'images',
        title: 'Images',
      },
    ])

    this.router = router
  }

  private logout(): void {
    this.user.logout()
    this.router.navigateToRoute('home')
  }

  private attached(): void {
    this.subscriptions.push(
      this.event.subscribe('ui:machines:connect', () => {
        this.showBottomPane = true
        this.createSplit()
      }),
      this.event.subscribe('api:user:logout', () => {
        this.showBottomPane = false
        this.destroySplit()
      }),
    )
  }

  private detached(): void {
    this.destroySplit()
    this.subscriptions.forEach((s) => s.dispose())
  }

  private createSplit(): void {
    if (this.split) {
      return
    }

    this.split = Split(['#top-pane', '#bottom-pane'], {
      direction: 'vertical',
      elementStyle: (dimension, size, gutterSize) => {
        return {
          'flex-basis': `calc(${size}% - ${gutterSize}px)`,
        }
      },
      gutter: (index, direction) => {
        const gutter = document.createElement('div')
        gutter.className = `column gutter gutter-${direction}`
        return gutter
      },
      gutterSize: 2,
      gutterStyle: (dimension, gutterSize) => {
        return {
          'flex-basis': `${gutterSize}px`,
        }
      },
      minSize: 25,
      onDragEnd: () => {
        this.event.publish('ui:pane:drag:end')
      },
      sizes: [70, 30],
    })
  }

  private destroySplit(): void {
    if (this.split) {
      this.split.destroy()
      this.split = undefined
    }
  }

}
