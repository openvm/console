import {autoinject} from 'aurelia-framework'
import {Router} from 'aurelia-router'
import {User} from './api/user'

@autoinject
export class Login {
  public username: string
  public password: string

  constructor(private router: Router, private user: User) {

  }

  public login(): void {
    this.user.login(this.username, this.password).then((res) => {
      this.router.navigate('machines')
    })
  }
}
