import { autoinject } from 'aurelia-framework'
import { Router } from 'aurelia-router'
import { User } from './../api/user'

@autoinject
export class Main {

  constructor(private user: User, private router: Router) {

  }

  private activate(): void {
    if (!this.user.loggedIn) {
      this.router.navigateToRoute('login')
    }
  }
}
