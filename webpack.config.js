const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const { AureliaPlugin } = require("aurelia-webpack-plugin");
const { optimize: { CommonsChunkPlugin } } = require("webpack");
const { TsConfigPathsPlugin, CheckerPlugin } = require("awesome-typescript-loader");

// config helpers:
const ensureArray = (config) => config && (Array.isArray(config) ? config : [config]) || [];
const when = (condition, config, negativeConfig) =>
  condition ? ensureArray(config) : ensureArray(negativeConfig);

// primary config:
const title = "OpenVM Console";
const outDir = path.resolve(__dirname, "dist");
const srcDir = path.resolve(__dirname, "src");
const baseUrl = "/";

const cssRules = [
  { loader: "css-loader" },
  {
    loader: "postcss-loader",
    options: { plugins: () => [require("autoprefixer")({ browsers: ["last 2 versions"] })]},
  },
];

/**
 * @return {webpack.Configuration}
 */
module.exports = ({production, server, extractCss, coverage} = {}) => ({
  resolve: {
    extensions: [".ts", ".js"],
    modules: [srcDir, "node_modules"],
  },

  devtool: production ? "source-map" : "eval-source-map",
  entry: "aurelia-bootstrapper",
  output: {
    path: outDir,
    publicPath: baseUrl,
    filename: "[name].[hash].bundle.js",
    sourceMapFilename: "[name].[hash].bundle.map",
    chunkFilename:  "[name].[hash].chunk.js",
  },
  devServer: {
    contentBase: outDir,
    // serve index.html for all 404 (required for push-state)
    historyApiFallback: true,
  },
  module: {
    rules: [
      // CSS required in JS/TS files should use the style-loader that auto-injects it into the website
      // only when the issuer is a .js/.ts file, so the loaders are not applied inside html templates
      {
        test: /\.css$/i,
        issuer: [{ not: [{ test: /\.html$/i }] }],
        use: extractCss ? ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: cssRules,
        }) : ["style-loader", ...cssRules],
      },
      {
        test: /\.css$/i,
        issuer: [{ test: /\.html$/i }],
        // CSS required in templates cannot be extracted safely
        // because Aurelia would try to require it again in runtime
        use: cssRules,
      },
      { test: /\.html$/i, loader: "html-loader" },
      { test: /\.ts$/i, loader: "awesome-typescript-loader" },
      { test: /\.json$/i, loader: "json-loader" },
      { test: /\.(ttf|eot|svg|png|otf|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/i, loader: "file-loader" },
    ],
  },
  plugins: [
    new AureliaPlugin({ aureliaApp: "app" }),
    new TsConfigPathsPlugin(),
    new CheckerPlugin(),
    new HtmlWebpackPlugin({
      template: "index.ejs",
      minify: production ? {
        removeComments: true,
        collapseWhitespace: true,
      } : undefined,
      metadata: {
        // available in index.ejs //
        title, server, baseUrl,
      },
    }),
    new CopyWebpackPlugin([
      { from: "static/favicon.ico", to: "favicon.ico" },
      { from: "config.json" },
    ]),
    ...when(extractCss, new ExtractTextPlugin({
      filename: production ? "[contenthash].css" : "[id].css",
      allChunks: true,
    })),
    ...when(production, new CommonsChunkPlugin({
      name: "common",
    })),
  ],
});
